Deze pagina beschrijft hoe je via Docker-compose op je eigen machine een omgeving kan opzetten om Django web applicaties te bouwen.

# Code downloaden
Clone deze repository of download alle source files via de knoppen boven deze instructies.

# Installeer docker-compose
Klik [hier](https://docs.docker.com/compose/install/) voor instructies

# Image bouwen
Deze stap gaat je Django project aanmaken en je Docker image bouwen. Dit moet je maar 1 keer doen.

## Bouw de image met docker-compose
`docker-compose run web django-admin startproject composeexample .`

Werk je op Linux, dan moet je `sudo` voor het vorige commando plaatsen.

## Stel de permissies in (enkel voor Linux)
`sudo chown -R $USER:$USER composeexample manage.py`

## Zorg ervoor dat de database instellingen correct zijn
- open `composeexample/settings.py` in een editor naar keuze
- voeg bovenaan deze file de volgende lijn toe: `import os`
- zoek naar `DATABASES =` en vervang door het volgende stuk code:

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_NAME'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

# Image starten
Dit moet je doen elke keer je je computer opnieuw opstart, of wanneer je de image manueel hebt gestopt.

Voer het volgende commando uit om de image te starten: `docker-compose up`


# Problemen?

## Het lukt niet om de Docker image te bouwen, omdat de versie niet ondersteund is.
Wanneer je een error ziet gelijkaardig aan deze:
```
ERROR: Version in "./docker-compose.yml" is unsupported. You might be seeing this error because you're using the wrong Compose file version. Either specify a supported version (e.g "2.2" or "3.3") and place your service definitions under the `services` key, or omit the `version` key and place your service definitions at the root of the file to use version 1.
For more on the Compose file format versions, see https://docs.docker.com/compose/compose-file/
```

dan betekent dat dat je een andere versie moet specifieren in je `docker-compose.yml` file alvorens opnieuw te bouwen.
In de error log worden een paar suggesties gegeven voor mogelijke versies. Vervang de versie in `docker-compose.yml` op lijn 1 door de versie die getoond wordt in de error boodschap.
Probeer nadien om de Docker image opnieuw te bouwen.
